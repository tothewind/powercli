import-module ImportExcel

$vc = Import-Excel -Path .\Site_Configuration_Workbook.xlsx  -WorksheetName Deployment_Params -StartRow 3 -EndRow 4 -StartColumn 2 -EndColumn 15
$infra = Import-Excel -Path .\Site_Configuration_Workbook.xlsx  -WorksheetName Deployment_Params -StartRow 7 -EndRow 8 -StartColumn 2 -EndColumn 10
$json = Get-Content .\embedded_vCSA_on_ESXi.json |ConvertFrom-Json

# ESXi Settings
$json = Get-Content .\embedded_vCSA_on_ESXi.json |ConvertFrom-Json
$json.new_vcsa.esxi.datastore = $vc.Deployment_Datastore
$json.new_vcsa.esxi.deployment_network = $vc.Deployment_Network
$json.new_vcsa.esxi.hostname = $vc.Deployment_Host
$json.new_vcsa.esxi.password = $vc.ESXi_Root_Pass

# Appliance Settings
$json.new_vcsa.appliance.deployment_option = $vc.vCenter_Size
$json.new_vcsa.appliance.name = $vc.FQDN

# Network Settings
$json.new_vcsa.network.ip = $vc.IP_Address
$json.new_vcsa.network.prefix = $vc.Network_CIDR
$json.new_vcsa.network.gateway = $vc.Gateway
$json.new_vcsa.network.system_name = $vc.FQDN
$json.new_vcsa.network.dns_servers = $infra.DNS1_IP,$infra.DNS2_IP

# OS Settings
$json.new_vcsa.OS.password = $vc.vCenter_Root_Pass
$json.new_vcsa.OS.ntp_servers = $infra.NTP1,$infra.NTP2
$json.new_vcsa.OS.ssh_enable = $infra.SSH_Enabled

# SSO Settings
$json.new_vcsa.SSO.password = $vc.SSO_Admin_Pass
$json.new_vcsa.SSO.domain_name = $vc.SSO_Domain_Name

# CEIP Set False

$json.CEIP.Settings.ceip_enabled = "false"

$json | ConvertTo-Json -Depth 25 | Out-File .\embedded_vCSA_on_ESXi.json