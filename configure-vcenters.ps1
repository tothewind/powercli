####################################################################################################
#   Title:      configure-vcenter.ps1
#   Author:     Sean Jabro
#   Created:    4-24-2019
#   Modified:   8-15-2019
####################################################################################################

#---------------------------#
#      	PARAMETERS			#
#---------------------------#

param(
	# User must enter the IP of the host to deploy to.
	[Parameter(
		Mandatory=$true,
		HelpMessage = "Please enter credentials for the vCenters"
		)
	]
	[pscredential]$vCenter_Admin_Creds,

	# User must enter the IP of the host to deploy to.
	[Parameter(
		Mandatory=$true,
		HelpMessage = "Please enter root credentials for the ESXi hosts"
		)
	]
	[pscredential]$ESXi_Root_Creds
	
)

#---------------------------#
# 	 Dot Source Helpers	    #  
#---------------------------#

. $PSScriptRoot\Helpers\functions.ps1
. $PSSCriptroot\Helpers\variables.ps1

#---------------------------#
#   Import Modules          #
#---------------------------#

$modules = 'Vmware.PowerCLI'

Write-toScreen "Attempting to import PowerShell modules. Please hold..." -Color Cyan

foreach ($module in $modules){
	try {
		Import-Module $module | Out-Null
	}catch{
		Write-toScreen "Failed to load Powershell module $module" -Color DarkRed
		Exit
	}
}

#---------------------------#
#   Instantiate Variables   #
#---------------------------#

$startTime = Get-Date

try {
	Write-toScreen "Connecting to management vCenter" -Color Cyan
	Connect-VIServer -Server $mgmtVcenter.FQDN -Credential $vCenter_Admin_Creds -ErrorAction Stop | Out-Null
}catch{
	"Unable to connect to vCenter. Exiting script"
	Exit
}

#####################################################################
#							MANAGEMENT								#
#####################################################################

Write-Break -Style DashedLine
Write-toScreen "<---------------------Configuring Management vCenter--------------------->"
Write-Break -Style DashedLine

#--Creating Management DC-#
try {
	Write-toScreen "Attempting to create datacenter" -Color Cyan
	$location = Get-Folder -NoRecursion
	New-Datacenter -Location $location -Name $mgmtVcenter.Datacenter -ErrorAction Stop | Out-Null
	Write-toScreen "Datacenter created successfully." -Color Green
}catch{
	Write-toScreen "Unable to create the datacenter. Exiting script" -Color DarkRed
	Write-Error $_.Exception
	Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false
	Exit
}

#-Create Management Cluster-#
try {
	Write-toScreen "Attempting to create cluster" -Color Cyan
	New-Cluster -Name $mgmtVcenter.Cluster -DrsEnabled -HAEnabled -Location $mgmtVcenter.Datacenter -ErrorAction Stop | Out-Null
	Write-toScreen "Cluster created successfully" -Color Green
}catch{
	Write-toScreen "Unable to create cluster. Exiting script." -Color DarkRed
	Write-Error $_.Exception
	Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false
	Exit
}

#-----Create Networking-----#

#__________VDS______________#
try {
	Write-toScreen "Attempting to create VDS" -Color Cyan
	New-VDSwitch -Location $mgmtVcenter.Datacenter -Mtu $mgmtVcenter.VDS_MTU -Name $mgmtVcenter.VDS -Version 6.5.0 -ErrorAction Stop | Out-Null
	Write-toScreen "VDS created successfully" -Color Green
}
catch {
	Write-toScreen "Failed to create VDS. Exiting script." -Color DarkRed
	Write-Error $_.Exception
	Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false
	Exit
}

#________Port Groups________#
Write-toScreen "Attempting to create port groups" -Color Cyan
$mgmtPortGroups = $mgmtVcenter.Port_Groups
foreach ($pg in $mgmtPortGroups){
	try {
		New-VDPortgroup -VDSwitch $mgmtVcenter.VDS -Name $pg."Portgroup Name" -VlanId $pg."VLAN #" -ErrorAction Stop | Out-Null
	}catch{
		Write-toScreen "Failed to create portgroup" -Color DarkRed
		Write-Error $_.Exception
		Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false
		Exit
	}
}
Write-toScreen "Port groups successfully created." -Color Green

#-----------Hosts-----------#

#_________Add Hosts_________#
$mgmtHosts = $mgmtVcenter.Hosts
Write-toScreen "Attempting to add hosts to vCenter" -Color Cyan
foreach ($esxHost in $mgmtHosts){
	if($esxHost.HostName){
		try {
			Add-VMHost -Name $esxHost.HostName -Location (get-datacenter $mgmtVcenter.Datacenter) -Credential $ESXi_Root_Creds -Force -RunAsync -Confirm:$false -ErrorAction Stop | Out-Null
		}catch{
			Write-toScreen "Failed to add hosts. Exiting script." -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
}

Write-toScreen "Successfully added hosts vCenter" -Color Green

#___Move Hosts to Cluster___#
Write-toScreen "Moving hosts in to the cluster." -Color Cyan
foreach ($esxHost in $mgmtHosts){
	if($esxHost.HostName){
		try {
			Move-VMHost -VMHost ($esxHost.HostName +"."+ $domain.Name) -Destination $mgmtVcenter.Cluster -ErrorAction Stop | Out-Null
		}catch{
			Write-toScreen "Failed to move hosts in to cluster. Exiting Script" -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
}

Write-toScreen "Successfully moved hosts in to cluster." -Color Green

#____Attach VDS to Hosts____#
Write-toScreen "Attempting to attach VDS to hosts" -Color Cyan
foreach ($esxHost in $mgmtHosts){
	if($esxHost.HostName){
		try {
			Get-VDSwitch -Name $mgmtVcenter.VDS | Add-VDSwitchVMHost -VMHost  ($esxHost.HostName +"."+ $domain.Name) -ErrorAction Stop
		}catch{
			Write-toScreen "Failed to add VDS to host. Exiting Script" -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
}

Write-toScreen "Successfully added VDS to hosts" -Color Green

#_____Migrate vmnic1_______#
Write-toScreen "Attempting to migrate vmnic1 to VDS" -Color Cyan
$mgmtVmHostNic1 = Get-VMHost | Get-VMHostNetworkAdapter -Physical -Name vmnic1
foreach ($vmHostNic in $mgmtVmHostNic1){
		try {
			Get-VDSwitch $mgmtVcenter.VDS | Add-VDSwitchPhysicalNetworkAdapter -VMHostPhysicalNic $vmHostNic -Confirm:$false -ErrorAction stop
		}catch{
			Write-toScreen "Failed to migrate vmnic1. Exiting script." -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
Write-toScreen "Successfully migrated vmnic1 to VDS" -Color Green

Write-toScreen "Attempting to migrate management vmk to VDS" -Color Cyan
foreach ($esxHost in $mgmtHosts){
	if($esxHost.HostName){
		try {
			$managementIbPg = $mgmtPortGroups | Where-Object {$_."Portgroup Name" -like "*ib*"}
			$vmk = Get-VMHostNetworkAdapter -Name vmk0 -VMHost ($esxHost.HostName + "." + $domain.name)
			Set-VMHostNetworkAdapter -PortGroup $managementIbPg."Portgroup Name" -VirtualNic $vmk -confirm:$false | Out-Null
		}catch{
			Write-toScreen "Failed to migrate management vmk. Exiting Script" -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $managementVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
}
Write-toScreen "Successfully migrated management vmk to VDS" -Color Green

#---------Create VMK Adapters--------#

Write-toScreen "Attempting to create VMK adapters on the hosts" -Color Cyan
foreach ($esxhost in $mgmthosts){
	if($esxHost.HostName){
		try {
			New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $mgmtPortGroups[1]."Portgroup Name"  -IP $esxhost."Out of Band IP" -SubnetMask $esxhost."Out of Band Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $mgmtVcenter.vds}) -ErrorAction Stop | Out-Null
			New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $mgmtPortGroups[2]."Portgroup Name"  -IP $esxhost."vMotion IP" -SubnetMask $esxhost."vMotion Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $mgmtVcenter.vds}) -ErrorAction Stop | Out-Null
			New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $mgmtPortGroups[3]."Portgroup Name"  -IP $esxhost."NFS IP" -SubnetMask $esxhost."NFS Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $mgmtVcenter.vds}) -ErrorAction Stop | Out-Null
			New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $mgmtPortGroups[4]."Portgroup Name"  -IP $esxhost."Replication IP" -SubnetMask $esxhost."Replication Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $mgmtVcenter.vds}) -ErrorAction Stop | Out-Null
			New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $mgmtPortGroups[5]."Portgroup Name"  -IP $esxhost."VXLAN IP" -SubnetMask $esxhost."VXLAN Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $mgmtVcenter.vds}) -ErrorAction Stop | Out-Null
			#New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $computePortGroups[6]."Portgroup Name"  -IP $esxhost."Uplink1 IP" -SubnetMask $esxhost."Uplink1 Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $computeVcenter.vds}) -ErrorAction Stop | Out-Null
			#New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $computePortGroups[7]."Portgroup Name"  -IP $esxhost."Uplink2 IP" -SubnetMask $esxhost."Uplink2 Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $computeVcenter.vds}) -ErrorAction Stop | Out-Null
		}catch{
			Write-Host "Unable to create VMK adapter on host."
			Write-Error $_.Exception
			Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
}

Write-toScreen "Successfully created vmk adapters on VDS" -Color Green

#_____Migrate VM NICs_____#


Try {
	Write-toScreen "Attempting to migrate the VM networks" -Color Cyan
	$networkAdapters = Get-VM | Get-NetworkAdapter -Name "Network Adapter 1"
	$mgmtPortGroup = Get-VDPortgroup -Name $mgmtPortGroups[0]."Portgroup Name"
	Set-NetworkAdapter -NetworkAdapter $networkAdapters -Portgroup $mgmtPortGroup -Confirm:$false -ErrorAction Stop | Out-Null
}catch{
	Write-toScreen "Failed to migrate VM networking. Exiting script." -Color DarkRed
	Write-Error $_.Exception
	Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false
	Exit
}

#_____Migrate vmnic0_______#
Write-toScreen "Attempting to migrate vmnic0 to VDS" -Color Cyan
$mgmtVmHostNic0 = Get-VMHost | Get-VMHostNetworkAdapter -Physical -Name vmnic0
foreach ($vmHostNic in $mgmtVmHostNic0){
		try {
			Get-VDSwitch $mgmtVcenter.VDS | Add-VDSwitchPhysicalNetworkAdapter -VMHostPhysicalNic $vmHostNic -Confirm:$false -ErrorAction Stop | Out-Null
		}catch{
			Write-toScreen "Failed to migrate vmnic0. Exiting script." -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}

Write-toScreen "Successfully migrated vmnic0 to VDS" -Color Green

Disconnect-VIServer $mgmtVcenter.FQDN -Force -Confirm:$false

Write-Break -Style DashedLine
Write-toScreen "<---------------------Management vCenter Complete--------------------->"
Write-Break -Style DashedLine

#####################################################################
#							COMPUTE									#
#####################################################################

try {
	Write-toScreen "Connecting to compute vCenter" -Color Cyan
	Connect-VIServer -Server $computeVcenter.FQDN -Credential $vCenter_Admin_Creds -ErrorAction Stop | Out-Null
}catch{
	Write-toScreen "Unable to connect to vCenter. Exiting script" -Color DarkRed
	Write-Error $_.Exception
	Exit
}

Write-Break -Style DashedLine
Write-toScreen "<---------------------Configuring Compute vCenter--------------------->"
Write-Break -Style DashedLine

#--Creating Compute DC-#
try {
	Write-toScreen "Attempting to create datacenter" -Color Cyan
	$location = Get-Folder -NoRecursion
	New-Datacenter -Location $location -Name $computeVcenter.Datacenter -ErrorAction Stop | Out-Null
	Write-toScreen "Datacenter created successfully." -Color Green
}catch{
	Write-toScreen "Unable to create the datacenter. Exiting script" -Color DarkRed
	Write-Error $_.Exception
	Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false
	Exit
}

#-Create Management Cluster-#
try {
	Write-toScreen "Attempting to create cluster" -Color Cyan
	New-Cluster -Name $computeVcenter.Cluster -DrsEnabled -HAEnabled -Location $computeVcenter.Datacenter -ErrorAction Stop | Out-Null
	Write-toScreen "Cluster created successfully" -Color Green
}catch{
	Write-toScreen "Unable to create cluster. Exiting script." -Color DarkRed
	Write-Error $_.Exception
	Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false
	Exit
}

#-----Create Networking-----#

#__________VDS______________#
try {
	Write-toScreen "Attempting to create VDS" -Color Cyan
	New-VDSwitch -Location $computeVcenter.Datacenter -Mtu $computeVcenter.VDS_MTU -Name $computeVcenter.VDS -Version 6.5.0 -ErrorAction Stop | Out-Null
	Write-toScreen "VDS created successfully" -Color Green
}
catch {
	Write-toScreen "Failed to create VDS. Exiting script." -Color DarkRed
	Write-Error $_.Exception
	Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false
	Exit
}

#________Port Groups________#
Write-toScreen "Attempting to create port groups" -Color Cyan
$computePortGroups = $computeVcenter.Port_Groups
foreach ($pg in $computePortGroups){
	try {
		New-VDPortgroup -VDSwitch $computeVcenter.VDS -Name $pg."Portgroup Name" -VlanId $pg."VLAN #" -ErrorAction Stop | Out-Null
	}catch{
		Write-toScreen "Failed to create portgroup" -Color DarkRed
		Write-Error $_.Exception
		Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false
		Exit
	}
}
Write-toScreen "Port groups successfully created." -Color Green

#-----------Hosts-----------#

#_________Add Hosts_________#
$computeHosts = $computeVcenter.Hosts 
Write-toScreen "Attempting to add hosts to vCenter" -Color Cyan
foreach ($esxHost in $computeHosts){
	if($esxHost.HostName) {
		try {
			Add-VMHost -Name $esxHost.HostName -Location (get-datacenter $computeVcenter.Datacenter) -Credential $ESXi_Root_Creds -Force -RunAsync -Confirm:$false -ErrorAction Stop | Out-Null
		}catch{
			Write-toScreen "Failed to add hosts. Exiting script." -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
}
Write-toScreen "Successfully added hosts vCenter" -Color Green

#___Move Hosts to Cluster___#
Write-toScreen "Moving hosts in to the cluster." -Color Cyan
foreach ($esxHost in $computeHosts){
	if($esxHost.HostName) {
		try {
			Move-VMHost -VMHost ($esxHost.HostName +"."+ $domain.Name) -Destination $computeVcenter.Cluster -ErrorAction Stop | Out-Null
		}catch{
			Write-toScreen "Failed to move hosts in to cluster. Exiting Script" -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
}
Write-toScreen "Successfully moved hosts in to cluster." -Color Green

#____Attach VDS to Hosts____#
Write-toScreen "Attempting to attach VDS to hosts" -Color Cyan
foreach ($esxHost in $computeHosts){
	if($esxHost.HostName){
		try {
			Get-VDSwitch -Name $computeVcenter.VDS |  Add-VDSwitchVMHost -VMHost ($esxHost.HostName +"."+ $domain.Name) -ErrorAction Stop
		}catch{
			Write-toScreen "Failed to add VDS to host. Exiting Script" -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
}

Write-toScreen "Successfully added VDS to hosts" -Color Green

#_____Migrate vmnic1_______#
Write-toScreen "Attempting to migrate vmnic1 to VDS" -Color Cyan
$computeVmHostNic1 = Get-VMHost | Get-VMHostNetworkAdapter -Physical -Name vmnic1
foreach ($vmHostNic in $computeVmHostNic1){
		try {
			Get-VDSwitch $computeVcenter.VDS | Add-VDSwitchPhysicalNetworkAdapter -VMHostPhysicalNic $vmHostNic -Confirm:$false -ErrorAction Stop | Out-Null
		}catch{
			Write-toScreen "Failed to migrate vmnic1. Exiting script." -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
Write-toScreen "Successfully migrated vmnic1 to VDS" -Color Green

Write-toScreen "Attempting to migrate management vmk to VDS" -Color Cyan
foreach ($esxHost in $computeHosts){
	if($esxHost.HostName){
		try {
			$computeIbPg = $computePortGroups | Where-Object {$_."Portgroup Name" -like "*ib*"}
			$vmk = Get-VMHostNetworkAdapter -Name vmk0 -VMHost ($esxHost.HostName + "." + $domain.name)
			Set-VMHostNetworkAdapter -PortGroup $computeIbPg."Portgroup Name" -VirtualNic $vmk -confirm:$false -ErrorAction Stop | Out-Null 
		}catch{
			Write-toScreen "Failed to migrate management vmk. Exiting Script" -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
}
Write-toScreen "Successfully migrated management vmk to VDS" -Color Green

#---------Create VMK Adapters--------#

Write-toScreen "Creating VMK adapters on the hosts" -Color Cyan
foreach ($esxhost in $computeHosts){
	if($esxHost.HostName){
		try {
			New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $computePortGroups[1]."Portgroup Name"  -IP $esxhost."Out of Band IP" -SubnetMask $esxhost."Out of Band Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $computeVcenter.vds}) -ErrorAction Stop | Out-Null
			New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $computePortGroups[2]."Portgroup Name"  -IP $esxhost."vMotion IP" -SubnetMask $esxhost."vMotion Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $computeVcenter.vds}) -ErrorAction Stop | Out-Null
			New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $computePortGroups[3]."Portgroup Name"  -IP $esxhost."NFS IP" -SubnetMask $esxhost."NFS Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $computeVcenter.vds}) -ErrorAction Stop | Out-Null
			New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $computePortGroups[4]."Portgroup Name"  -IP $esxhost."VXLAN IP" -SubnetMask $esxhost."VXLAN Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $computeVcenter.vds}) -ErrorAction Stop | Out-Null
			#New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $computePortGroups[5]."Portgroup Name"  -IP $esxhost."Uplink1 IP" -SubnetMask $esxhost."Uplink1 Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $computeVcenter.vds}) -ErrorAction Stop | Out-Null
			#New-VMHostNetworkAdapter -VMHost ($esxhost.HostName +"."+$domain.name) -PortGroup $computePortGroups[6]."Portgroup Name"  -IP $esxhost."Uplink2 IP" -SubnetMask $esxhost."Uplink2 Netmask"  -VirtualSwitch (Get-VirtualSwitch | Where-Object {$_.Name -like $computeVcenter.vds}) -ErrorAction Stop | Out-Null
		}catch{
			Write-toScreen "Unable to create VMK adapter on host." -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}
}

#_____Migrate vmnic0_______#
Write-toScreen "Attempting to migrate vmnic0 to VDS" -Color Cyan
$computeVmHostNic0 = Get-VMHost | Get-VMHostNetworkAdapter -Physical -Name vmnic0
foreach ($vmHostNic in $computeVmHostNic0){
		try {
			Get-VDSwitch $computeVcenter.VDS | Add-VDSwitchPhysicalNetworkAdapter -VMHostPhysicalNic $vmHostNic -Confirm:$false -ErrorAction stop | Out-Null
		}catch{
			Write-toScreen "Failed to migrate vmnic0. Exiting script." -Color DarkRed
			Write-Error $_.Exception
			Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false
			Exit
		}
	}

Write-toScreen "Successfully migrated vmnic0 to VDS" -Color Green

Disconnect-VIServer $computeVcenter.FQDN -Force -Confirm:$false

Write-Break -Style DashedLine
Write-toScreen "<---------------------Compute vCenter Complete--------------------->"
Write-Break -Style DashedLine

#---------------------------#
#     Script Cleanup        #
#---------------------------#

$endTime = Get-Date
$elapsedTime = $endTime - $startTime
$time = [math]::round($elapsedTime.TotalMinutes,2)
Write-Break -Style UnderscoreLine
Write-toScreen "Script complete." -Color DarkGreen
Write-toScreen "Total elapsed time in minutes: $time" -Color DarkGreen
Write-Break -Style UnderscoreLine