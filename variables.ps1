####################################################################################################
#   Title:      variables.ps1
#   Author:     Sean Jabro
#   Created:    8/14/2019 
#   Modified:   8/14/2019
####################################################################################################

<#
.SYNOPSIS
    Extracts data from the customized vmware-parameters.xlsx spreadsheet
.DESCRIPTION
    This script is a helper to any scripts that need to use data from the vmware.parameters.xlsx spreadsheet. 
    By dot sourcing this script from another script, you can pull in the variables in without having to massage the data much.
    The script creates a few new objects that act as the main set of variables to use in the scripts for deploying and configuring
        systems like NSX and vSphere.
    
    $domain         : This object will house the domain info supplied in spreadsheet.
    $mgmtVcenter    : This object will house the data for the management vCenter
    $computeVcenter : This object will house the data for the compute vCenter
    $mgmtNsx        : This object will house the data for the management NSX environment. Manager to ESGs and DLRs.
    $computeNsx     : This object will house the data for the compute NSX environment. Manager to ESGs and DLRs.
    $licensed       : This object will give you the VM name, application, version and License key supplied by the spreadsheet.
#>


#---------------------------#
#   Instatiate Variables    #
#---------------------------#

#-------File Locations-------#
$xlsx = Get-Item $PSSCriptroot\vmware-parameters.xlsx
$package = Open-ExcelPackage -Path $xlsx
$nsxOva = Get-Item $PSSCriptroot\OVAs\VMware-NSX-Manager-6.4.5-13282012.ova

#-----Collect ESXi Host Info----#
$mgmtHosts = Import-Excel -ExcelPackage $package -WorksheetName "Hosts and Networks" -StartRow 28 -EndRow 32 -StartColumn 2 -EndColumn 18
$computeHosts = Import-Excel -ExcelPackage $package -WorksheetName "Hosts and Networks" -StartRow 36 -EndRow 44 -StartColumn 2 -EndColumn 16

#----Build Domain Object----#
$domainInfo = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 10 -EndRow 21 -StartColumn 10 -EndColumn 10
$dns1 = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 8 -EndRow 8 -StartColumn 6 -EndColumn 6 -NoHeader
$dns2 = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 9 -EndRow 9 -StartColumn 6 -EndColumn 6 -NoHeader
$ntp1 = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 10 -EndRow 10 -StartColumn 6 -EndColumn 6 -NoHeader
$ntp2 = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 11 -EndRow 11 -StartColumn 6 -EndColumn 6 -NoHeader
$dnsZones = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 5 -EndRow 7 -StartColumn 10 -EndColumn 10
$smtpServer = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 16 -EndRow 18 -StartColumn 6 -EndColumn 6

$domain = @()
$domain += New-Object psobject -Property ([ordered]@{
    Name = $domainInfo[0].Value
    Alias = $domainInfo[1].Value
    DNS1 = $dns1.p1
    DNS2 = $dns2.p1
    NTP1 = $ntp1.p1
    NTP2 = $ntp2.p1
    Root_DNS_Zone = $dnsZones[0].Value
    Root_Base_DN_Users = $domainInfo[2].Value
    Root_Base_DN_Group = $domainInfo[3].Value
    Root_Server_Url = $domainInfo[4].Value
    SMTP_Server = $smtpServer[0].Value
    SMTP_Server_Port = $smtpServer[1].Value
})

#------Build vSphere Objects----#
$vCenterObjects = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 28 -EndRow 32 -StartColumn 9 -EndColumn 11
$mgmtPortGroups = Import-Excel -ExcelPackage $package -WorksheetName "Hosts and Networks" -StartRow 6 -EndRow 14 -StartColumn 2 -EndColumn 6
$computePortGroups = Import-Excel -ExcelPackage $package -WorksheetName "Hosts and Networks" -StartRow 17 -EndRow 24 -StartColumn 2 -EndColumn 6
$mgmtVcenterInfo = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 28 -EndRow 30 -StartColumn 6 -EndColumn 7
$computeVcenterInfo = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 31 -EndRow 34 -StartColumn 6 -EndColumn 7
$datastores = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 12 -EndRow 14 -StartColumn 6 -EndColumn 6

$distributedSwitches = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 33 -EndRow 37 -StartColumn 9 -EndColumn 10
$vdswitches = @()

$vdswitches += New-Object psobject -Property ([ordered]@{
	Name = $distributedSwitches[0].Value
	MTU = $distributedSwitches[1].Value
	}
)

$vdswitches += New-Object psobject -Property ([ordered]@{
	Name = $distributedSwitches[2].Value
	MTU = $distributedSwitches[3].Value
	}
)

##_______MGMT VCENTER________##
$mgmtVcenter = @()
$mgmtVcenter += New-Object psobject ([ordered]@{
    FQDN = ($mgmtVcenterInfo[0].Hostname + "." + $domain.Name)
    Hostname = $mgmtVcenterInfo[0].HostName
    IP = $mgmtVcenterInfo[0]."IP Address"
    Datacenter = $vCenterObjects[0].Value
    Cluster = $vCenterObjects[2].Value
    Datastore = $datastores[0].Value
    VDS = $distributedSwitches[0].Value
    VDS_MTU = $distributedSwitches[1].Value
    Port_Groups = $mgmtPortGroups
    Hosts = $mgmtHosts
})

##_____COMPUTE VCENTER________##
$computeVcenter = @()
$computeVcenter += New-Object psobject ([ordered]@{
    FQDN = ($computeVcenterInfo[0].Hostname + "." + $domain.Name)
    Hostname = $computeVcenterInfo[0].HostName
    IP = $computeVcenterInfo[0]."IP Address"
    Datacenter = $vCenterObjects[1].Value
    Cluster = $vCenterObjects[3].Value
    Datastore = $datastores[1].Value
    VDS = $distributedSwitches[2].Value
    VDS_MTU = $distributedSwitches[3].Value
    Port_Groups = $computePortGroups
    Hosts = $computeHosts
})

#------Build NSX Objects------#
$nsxFtpInfo = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 54 -EndRow 59 -StartColumn 6 -EndColumn 7    

$mgmtNsxmInfo = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 48 -EndRow 53 -StartColumn 6 -EndColumn 7    
$mgmtNsxmIdsAndRanges = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 66 -EndRow 70 -StartColumn 6 -EndColumn 7
$mgmtNsxEdgeInfo = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 72 -EndRow 74 -StartColumn 6 -EndColumn 7
$mgmtRoutingNode1 = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 75 -EndRow 78 -StartColumn 6 -EndColumn 7
$mgmtRoutingNode2 = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 79 -EndRow 82 -StartColumn 6 -EndColumn 7
$mgmtTopOfRackSwitch = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 83 -EndRow 89 -StartColumn 6 -EndColumn 7
$mgmtUDlr = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 90 -EndRow 96 -StartColumn 6 -EndColumn 7

$computeNsxmInfo = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 48 -EndRow 53 -StartColumn 10 -EndColumn 11
$computeNsxmIdsAndRanges = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 66 -EndRow 70 -StartColumn 10 -EndColumn 11
$computeNsxEdgeInfo = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 72 -EndRow 74 -StartColumn 10 -EndColumn 11
$computeRoutingNode1 = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 75 -EndRow 78 -StartColumn 10 -EndColumn 11
$computeRoutingNode2 = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 79 -EndRow 82 -StartColumn 10 -EndColumn 11
$computeTopOfRackSwitch = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 83 -EndRow 89 -StartColumn 10 -EndColumn 11
$computeUDlr = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 90 -EndRow 96 -StartColumn 10 -EndColumn 11
$computeDlr = Import-Excel -ExcelPackage $package -WorksheetName "Deploy Parameters" -StartRow 97 -EndRow 103 -StartColumn 10 -EndColumn 11  

##________MGMT NSX_________##
$mgmtNSX = @()
$mgmtNSX += New-Object psobject ([ordered]@{
    Manager_Hostname = $mgmtNsxmInfo[0].HostName
    Manager_IP = $mgmtNsxmInfo[0]."IP Address"
    Controller_IP_Pool_Start_Address = $mgmtNsxmInfo[1]."IP Address"
    Controller_IP_Pool_End_Address = $mgmtNsxmInfo[2]."IP Address"
    VTEP_IP_Pool_Start_Address = $mgmtNsxmInfo[3]."IP Address"
    VTEP_IP_Pool_End_Address = $mgmtNsxmInfo[4]."IP Address"
    FTP_Server = $nsxFtpInfo[0].Value
    FTP_Backup_Folder = $nsxFtpInfo[1].Value
    FTP_User = $nsxFtpInfo[3].Value
    FTP_Pass = $nsxFtpInfo[4].Value
    Segment_ID_Range_Start = $mgmtNsxmIdsAndRanges[0].Start
    Segment_ID_Range_End = $mgmtNsxmIdsAndRanges[0].End
    Universal_Segment_ID_Range_Start = $mgmtNsxmIdsAndRanges[1].Start
    Universal_Segment_ID_Range_End = $mgmtNsxmIdsAndRanges[1].End
    Multicast_Address_Range_Start = $mgmtNsxmIdsAndRanges[2].Start
    Multicast_Address_Range_End = $mgmtNsxmIdsAndRanges[2].End
    Universal_Multicast_Address_Range_Start = $mgmtNsxmIdsAndRanges[3].Start
    Universal_Multicast_Address_Range_End = $mgmtNsxmIdsAndRanges[3].End
    ESG_Autonomous_System_ID = $mgmtNsxEdgeInfo[0].Value
    ESG_BGP_Neighbor_Password = $mgmtNsxEdgeInfo[1].Value
    ESG_Routing_Node_1_Name = $mgmtRoutingNode1[0].Value
    ESG_Routing_Node_1_Uplink_1_IP = $mgmtRoutingNode1[1].Value
    ESG_Routing_Node_1_Uplink_2_IP = $mgmtRoutingNode1[2].Value
    ESG_Routing_Node_2_Name = $mgmtRoutingNode2[0].Value
    ESG_Routing_Node_2_Uplink_1_IP = $mgmtRoutingNode2[1].Value
    ESG_Routing_Node_2_Uplink_2_IP = $mgmtRoutingNode2[2].Value
    Top_of_Rack__1_IP = $mgmtTopOfRackSwitch[0].Value
    Top_of_Rack_1_Autonomous_System_ID = $mgmtTopOfRackSwitch[1].Value
    Top_of_Rack_1_BGP_Neighbor_Password = $mgmtTopOfRackSwitch[2].Value
    Top_of_Rack__2_IP = $mgmtTopOfRackSwitch[3].Value
    Top_of_Rack_2_Autonomous_System_ID = $mgmtTopOfRackSwitch[4].Value
    Top_of_Rack_2_BGP_Neighbor_Password = $mgmtTopOfRackSwitch[5].Value
    UDRL_Name = $mgmtUDlr[0].Value
    UDLR_ESG_Node_1_IP = $mgmtUDlr[1].Value
    UDLR_ESG_Node_2_IP = $mgmtUDlr[2].Value
    UDLR_ESG_Forwarding_IP = $mgmtUDlr[3].Value
    UDLR_ESG_Protocol_IP = $mgmtUDlr[4].Value
    UDLR_ESG_NETWORK_CIDR = $mgmtUDlr[5].Value

})

$computeNSX = @()
$computeNSX += New-Object psobject ([ordered]@{
    Manager_Hostname = $computeNsxmInfo[0].HostName
    Manager_IP = $computeNsxmInfo[0]."IP Address"
    Controller_IP_Pool_Start_Address = $computeNsxmInfo[1]."IP Address"
    Controller_IP_Pool_End_Address = $computeNsxmInfo[2]."IP Address"
    VTEP_IP_Pool_Start_Address = $computeNsxmInfo[3]."IP Address"
    VTEP_IP_Pool_End_Address = $computeNsxmInfo[4]."IP Address"
    FTP_Server = $nsxFtpInfo[0].Value
    FTP_Backup_Folder = $nsxFtpInfo[1].Value
    FTP_User = $nsxFtpInfo[3].Value
    FTP_Pass = $nsxFtpInfo[4].Value
    Segment_ID_Range_Start = $computeNsxmIdsAndRanges[0].Start
    Segment_ID_Range_End = $computeNsxmIdsAndRanges[0].End
    Universal_Segment_ID_Range_Start = $computeNsxmIdsAndRanges[1].Start
    Universal_Segment_ID_Range_End = $computeNsxmIdsAndRanges[1].End
    Multicast_Address_Range_Start = $computeNsxmIdsAndRanges[2].Start
    Multicast_Address_Range_End = $computeNsxmIdsAndRanges[2].End
    Universal_Multicast_Address_Range_Start = $computeNsxmIdsAndRanges[3].Start
    Universal_Multicast_Address_Range_End = $computeNsxmIdsAndRanges[3].End
    ESG_Autonomous_System_ID = $computeNsxEdgeInfo[0].Value
    ESG_BGP_Neighbor_Password = $computeNsxEdgeInfo[1].Value
    ESG_Routing_Node_1_Name = $computeRoutingNode1[0].Value
    ESG_Routing_Node_1_Uplink_1_IP = $computeRoutingNode1[1].Value
    ESG_Routing_Node_1_Uplink_2_IP = $computeRoutingNode1[2].Value
    ESG_Routing_Node_2_Name = $computeRoutingNode2[0].Value
    ESG_Routing_Node_2_Uplink_1_IP = $computeRoutingNode2[1].Value
    ESG_Routing_Node_2_Uplink_2_IP = $computeRoutingNode2[2].Value
    Top_of_Rack__1_IP = $computeTopOfRackSwitch[0].Value
    Top_of_Rack_1_Autonomous_System_ID = $computeTopOfRackSwitch[1].Value
    Top_of_Rack_1_BGP_Neighbor_Password = $computeTopOfRackSwitch[2].Value
    Top_of_Rack__2_IP = $computeTopOfRackSwitch[3].Value
    Top_of_Rack_2_Autonomous_System_ID = $computeTopOfRackSwitch[4].Value
    Top_of_Rack_2_BGP_Neighbor_Password = $computeTopOfRackSwitch[5].Value
    UDRL_Name = $computeUDlr[0].Value
    UDLR_ESG_Node_1_IP = $computeUDlr[1].Value
    UDLR_ESG_Node_2_IP = $computeUDlr[2].Value
    UDLR_ESG_Forwarding_IP = $computeUDlr[3].Value
    UDLR_ESG_Protocol_IP = $computeUDlr[4].Value
    UDLR_ESG_NETWORK_CIDR = $computeUDlr[5].Value
    DRL_Name = $computeDlr[0].Value
    DLR_ESG_Node_1_IP = $computeDlr[1].Value
    DLR_ESG_Node_2_IP = $computeDlr[2].Value
    DLR_ESG_Forwarding_IP = $computeDlr[3].Value
    DLR_ESG_Protocol_IP = $computeDlr[4].Value
    DLR_ESG_NETWORK_CIDR = $computeDlr[5].Value

})

#----Build License Object----#
$licenseInfo = Import-Excel -ExcelPackage $package -WorksheetName "Management Workloads" -StartRow 7 -EndRow 73 -StartColumn 2 -EndColumn 13 -HeaderName 'VM_Name','Application','OS','vCPUs','vRAM','Storage_GB','Swap_Size','Reservation','SRM_Protected','Backed_Up','Version','License_Key'
$licensed = @()
foreach ($prod in $licenseInfo){
    if($prod.license_key){$licensed += New-Object psobject -property ([ordered]@{
        VM_Name = $prod.VM_Name
        Application = $prod.Application
        Version = $prod.Version
        License = $prod.License_Key
        })
    }
}

#---------------------------#
#     Script Cleanup        #
#---------------------------#

Close-ExcelPackage -ExcelPackage $package -NoSave