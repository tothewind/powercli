
#---This funtion add on a timestamp to the front of the write-host console output.
function Write-toScreen (
    # The body of the text to be displayed
    [Parameter(ValueFromPipeline=$true)]
	[String]$Details,
    # Describe the color of the output
    [Parameter()]
    [ValidateSet('Black','Blue','Cyan','DarkBlue','DarkCyan','DarkGray','DarkGreen','DarkRed','Gray','Green','Magenta','Red','White','Yellow')]
    $Color
    )
{
    $LogDate = Get-Date -Format T
    Write-Host "$($LogDate): " -nonewline ;
    if ($Color -eq 'Black'){
        Write-Host $Details -ForegroundColor Black
    }elseif ($Color -eq 'Blue') {
        Write-Host $Details -ForegroundColor Blue
    }elseif ($Color -eq 'Cyan') {
        Write-Host $Details -ForegroundColor Cyan
    }elseif ($Color -eq 'DarkBlue') {
        Write-Host $Details -ForegroundColor DarkBlue
    }elseif ($Color -eq 'DarkCyan') {
        Write-Host $Details -ForegroundColor DarkCyan
    }elseif ($Color -eq 'DarkGray') {
        Write-Host $Details -ForegroundColor DarkGray
    }elseif ($Color -eq 'DarkGreen') {
        Write-Host $Details -ForegroundColor DarkGreen
    }elseif ($Color -eq 'DarkRed') {
        Write-Host $Details -ForegroundColor DarkRed
    }elseif ($Color -eq 'DarkYellow') {
        Write-Host $Details -ForegroundColor Gray
    }elseif ($Color -eq 'Green') {
        Write-Host $Details -ForegroundColor Green
    }elseif ($Color -eq 'Magenta') {
        Write-Host $Details -ForegroundColor Magenta
    }elseif ($Color -eq 'Red') {
        Write-Host $Details -ForegroundColor Red
    }elseif ($Color -eq 'White') {
        Write-Host $Details -ForegroundColor White
    }elseif ($Color -eq 'Yellow') {
        Write-Host $Details -ForegroundColor Yellow
    }else {
        Write-Host $Details
    }
}

#---This function is to create breaks in the console output with various symbols for the line breaks. It is completely superfluous and I use it to pretty up the output.  
function Write-Break {
    param (
        # The style used for the gap
        [Parameter()]
        [ValidateSet('DashedLine','EqualSignLine','UnderscoreLine')]
        $Style
    )
    if ($Style -eq 'DashedLine') {
        Write-Host "_                                                                                                  _" -ForegroundColor DarkCyan
        Write-Host "|--------------------------------------------------------------------------------------------------|" -ForegroundColor DarkCyan
        Write-Host "                                                                                                    " -ForegroundColor DarkCyan
    }elseif ($Style -eq 'EqualSignLine') {
        Write-Host "_                                                                                                  _ " -ForegroundColor DarkCyan
        Write-Host "|==================================================================================================|" -ForegroundColor DarkCyan
        Write-Host "                                                                                                    " -ForegroundColor DarkCyan
    }elseif ($Style -eq 'UnderscoreLine') {
        Write-Host "_                                                                                                  _" -ForegroundColor DarkCyan
        Write-Host "|__________________________________________________________________________________________________|" -ForegroundColor DarkCyan
        Write-Host "                                                                                                    " -ForegroundColor DarkCyan
    }else {
        Write-Host ""
        Write-Host ""
        Write-Host ""
    }
    
}

#---This function will convert a CIDR notation to a netmask. 
function convert-CidrToNetmask {
    param (
        $cidr
    )
    $netmask = '{0:D}.{1:D}.{2:D}.{3:D}' -f $('{0:X}' -f -bnot([Uint32][Math]::Pow(2,(32-$cidr))-1)-Split'(..)' | Where-Object {$_} | ForEach-Object {[byte]('0x'+$_)})
    Return $netmask
}


#---This function retrieves the certificate of the provided url. This can be used to parse the cert for the thumbprint for connections that require it.
# Function Test-WebServerSSL {  
#     # Function original location: http://en-us.sysadmins.lv/Lists/Posts/Post.aspx?List=332991f0-bfed-4143-9eea-f521167d287c&ID=60  
#     [CmdletBinding()]  
#         param(  
#             [Parameter(Mandatory = $true, ValueFromPipeline = $true, Position = 0)]  
#             [string]$URL,  
#             [Parameter(Position = 1)]  
#             [ValidateRange(1,65535)]  
#             [int]$Port = 443,  
#             [Parameter(Position = 2)]  
#             [Net.WebProxy]$Proxy,  
#             [Parameter(Position = 3)]  
#             [int]$Timeout = 15000,  
#             [switch]$UseUserContext  
#         )  
#     Add-Type @"  
#     using System;  
#     using System.Net;  
#     using System.Security.Cryptography.X509Certificates;  
#     namespace PKI {  
#         namespace Web {  
#             public class WebSSL {  
#                 public Uri OriginalURi;  
#                 public Uri ReturnedURi;  
#                 public X509Certificate2 Certificate;  
#                 //public X500DistinguishedName Issuer;  
#                 //public X500DistinguishedName Subject;  
#                 public string Issuer;  
#                 public string Subject;  
#                 public string[] SubjectAlternativeNames;  
#                 public bool CertificateIsValid;  
#                 //public X509ChainStatus[] ErrorInformation;  
#                 public string[] ErrorInformation;  
#                 public HttpWebResponse Response;  
#             }  
#         }  
#     }  
# "@  
#     $ConnectString = "https://$url`:$port"  
#     $WebRequest = [Net.WebRequest]::Create($ConnectString)  
#     $WebRequest.Proxy = $Proxy  
#     $WebRequest.Credentials = $null  
#     $WebRequest.Timeout = $Timeout  
#     $WebRequest.AllowAutoRedirect = $true  
#     [Net.ServicePointManager]::ServerCertificateValidationCallback = {$true}  
#     try {$Response = $WebRequest.GetResponse()}  
#     catch {}  
#     if ($WebRequest.ServicePoint.Certificate -ne $null) {  
#         $Cert = [Security.Cryptography.X509Certificates.X509Certificate2]$WebRequest.ServicePoint.Certificate.Handle  
#         try {$SAN = ($Cert.Extensions | Where-Object {$_.Oid.Value -eq "2.5.29.17"}).Format(0) -split ", "}  
#         catch {$SAN = $null}  
#         $chain = New-Object Security.Cryptography.X509Certificates.X509Chain -ArgumentList (!$UseUserContext)  
#         [void]$chain.ChainPolicy.ApplicationPolicy.Add("1.3.6.1.5.5.7.3.1")  
#         $Status = $chain.Build($Cert)  
#         New-Object PKI.Web.WebSSL -Property @{  
#             OriginalUri = $ConnectString;  
#             ReturnedUri = $Response.ResponseUri;  
#             Certificate = $WebRequest.ServicePoint.Certificate;  
#             Issuer = $WebRequest.ServicePoint.Certificate.Issuer;  
#             Subject = $WebRequest.ServicePoint.Certificate.Subject;  
#             SubjectAlternativeNames = $SAN;  
#             CertificateIsValid = $Status;  
#             Response = $Response;  
#             ErrorInformation = $chain.ChainStatus | ForEach-Object {$_.Status}  
#         }  
#         $chain.Reset()  
#         [Net.ServicePointManager]::ServerCertificateValidationCallback = $null  
#     } else {  
#         Write-Error $Error[0]  
#     }  
# }  
