####################################################################################################
#   Title:      {Enter Title}
#   Author:     {Enter Author Name}
#   Created:    (Enter Date Created) 
#   Modified:   {Enter Last Edit Date}
####################################################################################################

#---------------------------#
# 	 Dot Source Helpers	    #  
#---------------------------#

# . $PSSCriptroot\Helpers\params.ps1
# . $PSScriptRoot\Helpers\functions.ps1
# . $PSSCriptroot\Helpers\variables.ps1

#---------------------------#
#   Import Modules          #
#---------------------------#

#---------------------------#
#   Instatiate Variables    #
#---------------------------#

$startTime = Get-Date

    #####################################################
####                  SCRIPT BODY                        ####
    #####################################################


#---------------------------#
#     Script Cleanup        #
#---------------------------#

$endTime = Get-Date
$elapsedTime = $endTime - $startTime
$time = [math]::round($elapsedTime.TotalMinutes,2)
Write-Break -Style UnderscoreLine
Write-toScreen "Script complete." -Color DarkGreen
Write-toScreen "Total elapsed time in minutes: $time" -Color DarkGreen
Write-Break -Style UnderscoreLine